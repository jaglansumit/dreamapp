import {createStackNavigator} from 'react-navigation-stack';
import Login from '../screens/Login';
import LoginSignup from '../screens/LoginOrSignup';

const AuthNavigation = createStackNavigator(
  {
    Login: {screen: Login},
    LoginSignup: {screen: LoginSignup},
  },
  {
    initialRouteName: 'LoginSignup',
    headerMode: 'none',
  },
);

export default AuthNavigation;
