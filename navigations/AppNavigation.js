import React from 'react';
import Profile from '../screens/ProfileScreens/Profile';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Icon from 'react-native-ionicons';

const AppNavigation = createBottomTabNavigator(
  {
    Home: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: () => (
          <Icon
            style={{textAlignVertical: 'center', marginLeft: 20}}
            color="#0375FF"
            name="arrow-round-back"
          />
        ),
      },
    },
    // Settings: {
    //   screen: Settings,
    //   navigationOptions: {
    //     tabBarIcon: () => (
    //       <Icon name="bell" style={{color: '#0375FF'}} size={25} />
    //     ),
    //   },
    // },
  },
  {
    tabBarOptions: {
      showLabel: false,
      activeBackgroundColor: 'black',
      inactiveBackgroundColor: 'blue',
      activeTintColor: 'white',
    },
    initialRouteName: 'Home',
    headerMode: 'none',
  },
);

export default AppNavigation;
