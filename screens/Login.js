import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';

import ImagesHeader from '../components/header';
import * as constants from '../constants';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      message: '',
      showAlert: false,
      spinner: false,
    };
  }

  onLogin() {
    const {username, password} = this.state;
    if (!username || !password) {
      Alert.alert('Fill all Fields!');
    } else {
      this.handlePress(username, password);
    }
  }

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  async handlePress() {
    this.setState({spinner: true});
    fetch(constants.URL + 'api/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.username,
        password: this.state.password,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Response ======-------->', responseJson);
        if (responseJson.error) {
          this.setState({showAlert: true});
        } else {
          console.log('Token ----------------=====>', responseJson.token);
          AsyncStorage.setItem('token', responseJson.token);
          AsyncStorage.setItem('phone', responseJson.user.phone_number);
          AsyncStorage.setItem('email', responseJson.user.email);
          if (responseJson.user.email_verified_at === null) {
            AsyncStorage.setItem('email_verified_at', 'notVerified');
          } else {
            AsyncStorage.setItem('email_verified_at', 'Verified');
          }
          if (responseJson.user.phone_no_verified_at === null) {
            this.props.navigation.navigate('PhoneVerify');
            AsyncStorage.setItem('isLoggedIn', 'false');
          } else if (responseJson.user.email_verified_at === null) {
            this.props.navigation.navigate('EmailVerify');
            AsyncStorage.setItem('isLoggedIn', 'false');
          } else {
            AsyncStorage.setItem('isLoggedIn', 'true');
            this.props.navigation.navigate('App');
          }
        }
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.spinner}
          textContent={''}
          textStyle={styles.spinnerTextStyle}
        />
        <View style={styles.logoView}>
          <ImagesHeader />
          {/* <Image source={require('../assets/images/clinic_logo.png')} /> */}
        </View>
        <Text style={styles.screen}>Sign in</Text>
        <Text style={styles.tag}>
          Lorem Ipsum is simply dummy text of the printing and typesettings.
        </Text>
        <View style={styles.items}>
          <Text style={styles.label}> Email or Phone No: </Text>
          <TextInput
            value={this.state.username}
            onChangeText={(username) => this.setState({username})}
            placeholder={'Enter your email or phone no.'}
            style={styles.input}
          />
          <Text style={styles.label}> Password: </Text>
          <TextInput
            value={this.state.password}
            onChangeText={(password) => this.setState({password})}
            placeholder={'Enter your Password'}
            secureTextEntry={true}
            style={styles.input}
          />

          <TouchableOpacity
            onPress={this.onLogin.bind(this)}
            style={styles.button}>
            <Text style={styles.submitText}>Login </Text>
          </TouchableOpacity>

          <View
            onStartShouldSetResponder={() =>
              this.props.navigation.navigate('ResetPassword')
            }>
            <Text style={styles.tagThree}>Reset Password</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  input: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 40,
    marginRight: 40,
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    height: 50,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  items: {
    marginTop: 60,
    textAlign: 'left',
  },
  label: {
    marginLeft: 40,
    fontSize: 20,
    fontWeight: 'bold',
  },
  screen: {
    marginTop: 40,
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  tag: {
    marginTop: 10,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: 'grey',
    textAlign: 'center',
  },
  submitText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  logoView: {
    // alignItems: 'flex-end',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  tagThree: {
    marginTop: 40,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#0158ff',
    fontWeight: '300',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});
