import AsyncStorage from '@react-native-community/async-storage';
import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

export default class LoginSignup extends Component {
  async componentDidMount() {
    const d = await AsyncStorage.getItem('isLoggedIn');
    console.log('Logged IN ----------------->', d);
    if (d === 'true') {
      this.props.navigation.navigate('App');
      console.log('Logged IN  If condition----------------->', d);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={require('../clinic_tag.png')} />
        <View style={styles.bottom}>
          <Text style={styles.tagMain}>Welcome</Text>
          <Text style={styles.tagSecond}>to Swift Clinic</Text>
          <Text style={styles.tag}>
            Lorem Ipsum is simply dummy text of the printing and typesettings.
          </Text>
          <View
            onStartShouldSetResponder={() =>
              this.props.navigation.navigate('Login')
            }
            style={styles.buttonLogin}>
            <Text style={styles.Text}>Sign In </Text>
          </View>
          <View
            onStartShouldSetResponder={() =>
              this.props.navigation.navigate('Login')
            }
            style={styles.button}>
            <Text style={styles.tagThree}>Sign Up </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    height: 50,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 7,
    color: 'white',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  buttonLogin: {
    borderRadius: 9,
    marginLeft: 80,
    height: 50,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 55,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  image: {
    marginTop: 60,
  },
  tag: {
    marginTop: 40,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fefefe',
    fontWeight: '300',
    textAlign: 'center',
  },
  tagThree: {
    marginTop: 40,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fefefe',
    fontWeight: '300',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  tagMain: {
    marginTop: 30,
    fontSize: 55,
    paddingLeft: 50,
    paddingRight: 50,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  tagSecond: {
    marginTop: 10,
    fontSize: 20,
    paddingLeft: 50,
    paddingRight: 50,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  bottom: {
    marginTop: 70,
    backgroundColor: '#0158ff',
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  Text: {
    fontWeight: 'bold',
  },
});
