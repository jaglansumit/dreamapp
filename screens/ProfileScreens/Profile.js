import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class Profile extends Component {
  render() {
    return (
      <View styles={styles.container}>
        <Text>Home Screen</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
});
